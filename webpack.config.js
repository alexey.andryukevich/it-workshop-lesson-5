const path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // mode: 'production',
    entry: './src/index.js', // входной файл
    output: { //результат работы
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
          {
            test: /\.css$/i,
            use: ['style-loader', 'css-loader']
          },
          {
            test: /\.s[ac]ss$/i,
            use: ['style-loader','css-loader','sass-loader']
          },          
          {
            test: /\.m?js$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env']
              }
            }
          },
          {
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/,
          },
          {
            test: /\.(png|svg|jpg|gif)$/,
            use: ['file-loader?name=images/[name].[ext]']
          }
        ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ],
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist')
    },
    plugins: [
        new HtmlWebpackPlugin({template: './src/index.html'})
    ]
}